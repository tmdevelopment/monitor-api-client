# PHP Monitor G5 Api Client

Reccomend to use strict_types


Samples are described in examples.php

Possible query options are described in here
https://api.monitor.se/articles/v1/queries/query-options/index.html

These are usable for get requests. Current default TOP value is 25.

```
$client->Inventory_Parts->get(12345);

$client->Inventory_Parts->filter("PartNumber eq 'LO1.IL.AM22H2.R0'")->get();

$client->Inventory_Parts->top(10)->skip(7)->expand("PartPlanningInformations")->get();

```

## Mapping to a type

Use `mapTo()` function. NB! most of the types are untested

```
$currencies = $g5->mapTo(Currency::class)->Common_Currencies()->get();
```

## Custom DocBlock Annotation logic

* `@required` - json_encode throws an error if it has not been, initialized
* `@maxlength 123` - json_encode throws an error if its value exceeds the numeric value

## Installing

Add repository
```
composer config repositories.tmdevelopment-monitor-api-client vcs https://bitbucket.org/tmdevelopment/monitor-api-client.git
```

```
composer require tmdevelopment/monitor-api-client
```

## Developing

Add repository via symlink

```
composer config repositories.tmdevelopment-monitor-api-client '{"type": "path", "url": "c:/git/tmdevelopment/monitor-api-client","options": {"symlink": true}}'
```
and then require it
```
composer require tmdevelopment/monitor-api-client
```

## New stable version

Change the version number in `composer.json` and tag it in git.

`git tag -a v1.0.0 -m "New release"`

`git push origin v1.0.0`
