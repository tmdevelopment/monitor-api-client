<?php

namespace Monitor;

class CommunicationAddressType extends Constants
{
    public const Phone = 0;
    public const Fax = 1;
    public const Email = 2;
    public const CellPhone = 3;
}
