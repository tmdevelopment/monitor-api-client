<?php

namespace Monitor;

class PartConfigurationUpdateType
{

    public const Variable = 0;
    public const SelectionGroupRow = 1;
    public const MainPart = 2;
}
