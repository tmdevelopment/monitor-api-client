<?php

namespace Monitor;

class OrderRowType
{
    public const Part = 1;
    public const Additional = 2;
    public const Sum = 3;
    public const FreeText = 4;
}
