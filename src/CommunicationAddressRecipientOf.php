<?php

namespace Monitor;

class CommunicationAddressRecipientOf
{

    public const None = 0;
    public const Invoice = 1;
    public const Reminder = 2;
    public const OrderConfirmation = 4;
    public const DispatchAdvice = 8;
    public const ShippingAgentAdvice = 16;
    public const Quote = 512;
    public const ShippingAgentSmsAdvice = 1024;
    public const CaseConfirmation = 16384;
    public const CaseResponse = 32768;
    public const Proforma = 131072;
    public const ProjectReportExternal = 262144;
    public const CustomerDeliveryNote = 1048576;
    public const TraceabilityProtocol = 2097152;
    public const BlanketOrderSales = 8388608;
}
