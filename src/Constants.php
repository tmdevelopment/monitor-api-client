<?php

namespace Monitor;

class Constants implements \JsonSerializable
{
    public function jsonSerialize()
    {
        $class = get_class($this);
        $reflectedClass = new \ReflectionClass($class);
        return $reflectedClass->getConstants();
    }
}
