<?php

namespace Monitor;

use Exception;
use Monitor\API\Infrastructure\StringInput;

class Base implements \JsonSerializable
{
    public function __set(string $name, $value)
    {
        throw new \Exception("Variable '$name' does not exist in '" . get_class($this) . "'");
    }

    public function jsonSerialize()
    {
        $class = get_class($this);
        $reflectedClass = new \ReflectionClass($class);
        foreach ($reflectedClass->getProperties() as $property) {
            $docblock = $property->getDocComment();
            if (preg_match_all('/@(?<label>\w+)\s+(?<value>.*)/m', $docblock, $matches)) {
                $isInitialized = $property->isInitialized($this) ;
                $result = array_combine($matches[1], $matches[2]);
                if (array_key_exists("required", $result)) {
                    if ($isInitialized == false) {
                        throw new Exception("'$property->class' has unitialized required property '$property->name'");
                    }
                }
                if ($isInitialized && array_key_exists("maxlength", $result) && is_numeric(($maxlength = $result["maxlength"]))) {
                    $val = $property->getValue($this);
                    if($val instanceof StringInput){
                        $val = $val->Value;
                    }
                    if (is_object($val) == false && mb_strlen($val) > intval($maxlength)) {
                        throw new Exception("'$property->class' '$property->name' value length exceeds maxlength annotation");
                    }
                }
            }
            $type = $property->getType()->getName();
            if ($type == "int") {
                /**
                 * not sure anymore, if the reason was in 32bit php or Monitor API can not deserialize numeric 64-bit int. 
                 * If the latter, would neet JSON_BIGINT_AS_STRING for encode
                 */
                if ($property->isInitialized($this) && $property->getValue($this) > 2147483647) {
                    throw new Exception("Datatype design error, $class : $property->name cannot be an integer");
                }
            }
        }
        return $this;
    }
}
