<?php

namespace Monitor;

use Exception;
use JsonMapper;
use Requests;
use Requests_Exception;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;

/**
 * 
 * GET
 * @method Inventory_Parts() Parts
 * @method Inventory_PartCodes() PartCodes
 * @method Sales_Customers() Customers
 * @method Common_Currencies() Currencies
 * @method Sales_CustomerOrderTypes() CustomerOrderTypes
 * 
 * POST
 * @method Common_PartConfigurations_Exists bool
 * @method Common_PartConfigurations_Poke(PokePartConfigurationSession $pokePartConfigurationSession) bool
 * @method Common_PartConfigurations_Update(API\Common\Commands\PartConfigurations\UpdatePartConfiguratio $updatePartConfiguration) PartConfigurationState
 * @method Sales_CustomerOrders_Create(CreateCustomerOrder $customerOrder) ??
 * @method Sales_PartConfigurations_CreateForCustomerOrder(CreateForCustomerOrder $partConfiguration) PartConfigurationState
 * 
 */

class Client
{
    private string $host;
    private string $company;
    private string $username;
    private string $password;

    private array $headers = array("Accept" => "application/json", "Content-Type" => "application/json");
    private array $options = array("verify" => false, "timeout" => 100);

    private string $lang = "en";

    public const TOP = 25;

    //https://api.monitor.se/articles/v1/queries/query-options/index.html
    private array $queryOptions = array('$select', '$expand', '$orderby', '$top', '$skip', '$filter');

    private $args;

    private $mapTo = null;

    private Logger $logger;

    private $lastResponse;

    public function __construct($host, $company, $username, $password, $lang = null)
    {
        if (empty($host) || empty($company) || empty($username) || empty($password)) {
            throw new Exception("Some of the connection params are empty");
        }

        $this->host = $host;
        $this->company = $company;
        $this->username = $username;
        $this->password = $password;

        if (isset($lang)) {
            $this->lang = $lang;
        }

        $this->logger = new Logger('monitor-api-client');
        $this->logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

        $logFilename = join(DIRECTORY_SEPARATOR, array(__DIR__, "log", "monitor-api-client.log"));
        $this->logger->pushHandler(new RotatingFileHandler($logFilename, 31, Logger::DEBUG, true, 0664));
        $this->login();
    }

    private function send($path, $data, $type = Requests::GET, $options = array())
    {
        $timings = [];
        $startTime = microtime(true);
        $url = join("/", array($this->host, $this->lang, $this->company, $path));

        $serializedData = json_encode($data);
        $timings["serializing_request_body"] = microtime(true) - $startTime;
        $startTime = microtime(true);
        $this->logger->info("'$type' request to '$url' " . $serializedData);

        if ($type == Requests::POST && is_string($data) == false) {
            $data = $serializedData;
        }
        try {
            $this->lastResponse = Requests::request($url, $this->headers, $data, $type, $this->options + $options);
        } catch (Requests_Exception $ex) {
            $this->logger->error($ex->getMessage());
            if (strpos($ex->getMessage(), "cURL error 28") !== false) {
                sleep(5);
            }
            $this->lastResponse = Requests::request($url, $this->headers, $data, $type, $this->options + $options);
        }
        $timings["request"] = microtime(true) - $startTime;
        $startTime = microtime(true);
        $this->logger->debug("Response '{$this->lastResponse->status_code}', timing: '{$timings["request"]}' Body: '{$this->lastResponse->body}'");

        if ($this->lastResponse->status_code === 200) {
            if ($this->lastResponse->headers->getValues("content-type")[0] == "application/json; charset=utf-8") {
                $this->lastResponse->parsedBody = json_decode($this->lastResponse->body);
            }
        } else {
            $this->logger->error("Response '{$this->lastResponse->status_code}' Body: '{$this->lastResponse->body}'");
            throw new \Exception($this->lastResponse->body);
        }

        $startTime = microtime(true);
        $timings["deserializing_response_body"] = microtime(true) - $startTime;

        if ($this->mapTo != null && class_exists($this->mapTo)) {
            try {
                if (is_array($this->lastResponse->parsedBody)) {
                    $this->lastResponse->mappedTo = (new JsonMapper())->mapArray(
                        $this->lastResponse->parsedBody,
                        array(),
                        $this->mapTo
                    );
                } else {
                    $mapTo = $this->mapTo;
                    $this->lastResponse->mappedTo = (new JsonMapper())->map($this->lastResponse->parsedBody, new $mapTo());
                }
            } catch (Exception $e) {
                $msg = "Json Mapping failed to '$this->mapTo'  " . $e->getMessage() . "Body: '{$this->lastResponse->body}'";
                $this->logger->error($msg);
                throw new $msg;
            }
        }
        $timings["mapping_to_object"] = microtime(true) - $startTime;
        $this->logger->debug("Timings: " . json_encode($timings));
        $this->args = array();
        $this->mapTo = null;
        return $this->lastResponse;
    }

    public function get(?int $id = null)
    {

        $data = array();

        if (isset($this->args["path"]) == false) {
            throw new Exception("Path not provided");
        }

        if (isset($id)) {
            $this->args["path"] .= "/$id";

            if (isset($this->args['$select'])) {
                $data['$select'] = $this->args['$select'];
            }
            if (isset($this->args['$expand'])) {
                $data['$expand'] = $this->args['$expand'];
            }
        } else {
            $data = array();
            foreach ($this->queryOptions as $queryOption) {
                if (isset($this->args[$queryOption])) {
                    $data[$queryOption] = $this->args[$queryOption];
                }
            }
            if (isset($data['$top']) == false) {
                $data['$top'] = self::TOP;
            }
        }

        $response = $this->send("api/v1/" . $this->args["path"], $data);
        return isset($response->mappedTo) ? $response->mappedTo : $response->parsedBody;
    }

    public function login()
    {
        $body = new \stdClass();
        $body->Username = $this->username;
        $body->Password = $this->password;
        $body->ForceRelogin = true;
        $loginResponse = $this->send("login", $body, Requests::POST);
        $this->headers["X-Monitor-SessionId"] = $loginResponse->headers['x-monitor-sessionid'];
    }

    public function setLang(string $lang)
    {
        $this->lang = $lang;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function __call($name, $args)
    {
        $this->args = array("path" => str_replace("_", "/", $name));

        if (count($args) == 0) {
            return $this;
        } else if (count($args) == 1) {
            $className = get_class($args[0]);
            if (strpos($className, __NAMESPACE__) === 0) {
                $response = $this->send("api/v1/" . $this->args["path"], $args[0], Requests::POST);
                return isset($response->mappedTo) ? $response->mappedTo : $response->parsedBody;
            }
        }
        throw new Exception("Invalid arguments?");
    }

    /**
     * @param string $value Comma seperated fields, without spaces
     */
    public function select(string $value)
    {
        $this->args['$' . strtolower(__FUNCTION__)] = $value;
        return $this;
    }

    public function expand(string $value)
    {
        $this->args['$' . strtolower(__FUNCTION__)] = $value;
        return $this;
    }

    public function orderby(string $value)
    {
        $this->args['$' . strtolower(__FUNCTION__)] = $value;
        return $this;
    }

    public function top(int $value)
    {
        $this->args['$' . strtolower(__FUNCTION__)] = $value;
        return $this;
    }

    public function skip(int $value)
    {
        $this->args['$' . strtolower(__FUNCTION__)] = $value;
        return $this;
    }

    public function mapTo(string $className)
    {
        $this->mapTo = $className;
        return $this;
    }

    /**
     * https://api.monitor.se/articles/v1/queries/query-options/filter-functions.html
     */
    public function filter(string $value)
    {
        $this->args['$' . strtolower(__FUNCTION__)] = $value;
        return $this;
    }

    /**
     * @var BatchCommandEntity[] $commands
     */
    public function Batch(array $commands){
        $response = $this->send("api/v1/Batch", $commands, Requests::POST); 
        return $response->parsedBody;
    }
}
