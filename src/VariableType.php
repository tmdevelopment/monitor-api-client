<?php

namespace Monitor;

class VariableType extends Constants
{
    public const String = 0;
    public const Numeric = 1;
    public const Boolean = 2;
    public const DateTime = 3;
}
