<?php

namespace Monitor;

trait UnserializeCast
{

    public function __unserialize(array $data): void
    {

        $class = get_class($this);
        $reflectedClass = new \ReflectionClass($class);

        foreach ($data as $key => $val) {
            if ($reflectedClass->hasProperty($key) == false) {
                continue;
            }
            $property = $reflectedClass->getProperty($key);
            $type = $property->getType();
            if ($type) {
                $typeName = $type->getName();
                $valType = gettype($val);
                $valType = $valType == "integer" ? "int" : $valType;
                if ($typeName !== $valType) {
                    switch ($typeName) {
                        case "int":
                            $val = intval($val);
                            break;
                        case "string":
                            $val = (string)$val;
                            break;
                    }
                }
            }
            $this->{$key} = $val;
        }
    }
}
