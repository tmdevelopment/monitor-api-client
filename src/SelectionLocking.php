<?php

namespace Monitor;

class SelectionLocking extends Constants
{
    public const None = 0;
    public const Lock = 1;
    public const Unlock = 2;
}
