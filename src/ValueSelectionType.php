<?php

namespace Monitor;

class ValueSelectionType extends Constants
{
    public const Initialization =  0;
    public const Manual =  1;
    public const Formula =  2;
    public const LinkedSelection =  3;
    public const Fallback =  4;
}
