<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.BooleanInput.html
 */
class BooleanInput extends Base
{

	/**
	 * The nullable boolean value to update the property with
	 * 
	 */
	public ?bool $Value;

	public function __construct(bool $value)
	{
		$this->Value = $value;
	}
}
