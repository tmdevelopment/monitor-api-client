<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.NotNullDecimalInput.html
 */
class NotNullDecimalInput extends Base{

	/**
	 * The decimal value to update the property with.
	 */
	public float $Value;

}