<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.NotNullBooleanInput.html
 */
class NotNullBooleanInput extends Base
{

    /**
     * The boolean value to update the property with
     * 
     */
    public bool $Value;

    public function __construct($value)
    {
        $this->Value = (bool)$value;
    }
}
