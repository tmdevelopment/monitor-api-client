<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.NotNullDecimalInput.html
 */
class NotNullLongInput extends Base
{

	/**
	 * The decimal value to update the property with.
	 */
	public string $Value;

	public function __construct($value)
	{
		$this->Value = (string)$value;
	}
}
