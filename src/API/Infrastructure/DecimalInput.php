<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.DecimalInput.html
 */
class DecimalInput extends Base{

	/**
	 * The nullable decimal value to update the property with
	 * 
	 */
	public ?float $Value;

}