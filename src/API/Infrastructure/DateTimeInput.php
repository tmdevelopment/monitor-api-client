<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;
use Monitor\DateTimeOffset;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.DateTimeInput.html
 */
class DateTimeInput extends Base{

	/**
	 * The nullable date/time value to update the property with
	 * 
	 */
	public ?DateTimeOffset $Value;

}