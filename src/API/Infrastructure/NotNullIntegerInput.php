<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.NotNullIntegerInput.html
 */
class NotNullIntegerInput extends Base
{

    /**
     * The 32-bit integer value to update the property with
     * 
     */
    public int $Value;

    public function __construct($value)
    {
        $this->Value = (int)$value;
    }
}
