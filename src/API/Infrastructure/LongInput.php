<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.LongInput.html
 */
class LongInput extends Base{

	/**
	 * The nullable 64-bit integer value to update the property with
	 * 
	 */
	public ?string $Value;

	public function __construct($value)
	{
		$this->Value = (string)$value;
	}

}