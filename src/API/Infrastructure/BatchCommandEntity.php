<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.BatchCommandEntity.html
 */
class BatchCommandEntity extends Base{

	/**
	 * The API-local URL to the command that should be invoked
	 * For example /Common/Commands/CreateComment.
	 */
	public string $Path;

	/**
	 * The JSON-encoded instance of the command that should be invoked
	 * 
	 */
	public Object $Body;

	/**
	 * The name of the property to forward from the response of the commandto the next command in the batch
	 * 
	 */
	public string $ForwardPropertyName;

	/**
	 * The name of the property that should receive the forwarded property from theprevious command in the batch
	 * 
	 */
	public string $ReceivingPropertyName;

}