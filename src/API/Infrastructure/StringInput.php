<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.StringInput.html
 */
class StringInput extends Base
{

	/**
	 * The string value to update the property with
	 * 
	 */
	public string $Value;

	public function __construct(string $value)
	{
		$this->Value = $value;
	}
}
