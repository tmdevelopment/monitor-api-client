<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.IntegerInput.html
 */
class IntegerInput extends Base{

	/**
	 * The nullable 32-bit integer value to update the property with
	 * 
	 */
	public ?int $Value;

}