<?php

namespace Monitor\API\Infrastructure;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Infrastructure.ApiValidationResult.html
 */
class ApiValidationResult extends Base{

	/**
	 * The validation kind of the result
	 * Valid: 0Warning: 1Error: 2
	 */
	public int $Kind;

	/**
	 * The message returned by the validation
	 * 
	 */
	public string $Message;

}