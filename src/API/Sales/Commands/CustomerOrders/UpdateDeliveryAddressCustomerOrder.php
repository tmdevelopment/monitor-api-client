<?php

namespace Monitor\API\Sales\Commands\CustomerOrders;

use Monitor\API\Common\Commands\Shared\SetAddressCountry;
use Monitor\API\Infrastructure\LongInput;
use Monitor\API\Infrastructure\NotNullLongInput;
use Monitor\API\Infrastructure\StringInput;
use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.CustomerOrders.UpdateDeliveryAddressCustomerOrder.html
 */
class UpdateDeliveryAddressCustomerOrder extends Base
{

    /**
     * The business key identifier of the customer order to update the deliveryaddress for
     * References CustomerOrdersMandatory
     * @required
     */
    public string $CustomerOrderId;

    /**
     * The addressee of the address
     * @maxlength 100
     */
    public StringInput $Addressee;

    /**
     * The first field of the address
     * @maxlength 50
     */
    public StringInput $Field1;

    /**
     * The second field of the address
     * @maxlength 50
     */
    public StringInput $Field2;

    /**
     * The third field of the address
     * @maxlength 50
     */
    public StringInput $Field3;

    /**
     * The fourth field of the address
     * @maxlength 50
     */
    public StringInput $Field4;

    /**
     * The fifth field of the address
     * @maxlength 50
     */
    public StringInput $Field5;

    /**
     * The locality of the address
     * 
     */
    public StringInput $Locality;

    /**
     * The region of the address
     * 
     */
    public StringInput $Region;

    /**
     * The postal code of the address
     * 
     */
    public StringInput $PostalCode;

    /**
     * The business key identifier of the language code
     * References LanguageCodes.
     */
    public NotNullLongInput $LanguageId;

    /**
     * The business key identifier of the form report translation group
     * References FormReportTranslationGroup.
     */
    public LongInput $FormReportTranslationGroupId;

    /**
     * The business key identifier of the postal code
     * References PostalCode.
     */
    public LongInput $PostalCodeId;

    /**
     * Sets the country of the address
     * 
     */
    public SetAddressCountry $SetAddressCountry;
}
