<?php

namespace Monitor\API\Sales\Commands\CustomerOrders;

use Monitor\API\Infrastructure\NotNullDecimalInput;
use Monitor\Base;
use Monitor\DateTimeOffset;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.CustomerOrders.AddCustomerOrderRow.html
 */
class AddCustomerOrderRow extends Base{

	/**
	 * The business key identifier of the customer order to add a row to.            References CustomerOrdersMandatory
	 * @required - can be null if added directly to order, use zero then?
	 */
	public int $CustomerOrderId;

	/**
	 * The business key identifier of the part to set for the row.            References Parts
	 */
	public ?int $PartId;

	/**
	 * The session id of the part configuration to apply for the customer order row.Introduced in version 2.33
	 */
	public ?string $PartConfigurationSessionId;

	/**
	 * The additional part code to set for the row.Max length is 20
	 */
	public string $AdditionalPartCode;

	/**
	 * The part description to set for the row.Max length is 80The part description by default
	 */
	public string $PartDescription;

	/**
	 * The text row data to set for the row.Max length is 0
	 */
	public string $TextRowData;

	/**
	 * The quantity to set for the row either in the unit specified by UnitId or the part's customer order unit.
	 */
	public float $OrderedQuantity;

	/**
	 * The unit price to set for the row.Calculated by MONITOR by default
	 */
	public ?float $Price;

	/**
	 * The setup price to set for the row.Calculated by MONITOR by default
	 */
	public ?float $SetupPrice;

	/**
	 * The business key identifier of the warehouse to set for the row.            References WarehousesSames as the customer order by default
	 */
	public ?int $WarehouseId;

	/**
	 * The order row type to set for the row.
     * Part: 1
     * Additional: 2
     * Sum: 3
     * FreeText: 4
     * 
     * @required
	 */
	public int $OrderRowType;

	/**
	 * The delivery date to set for the row.
	 */
	public ?DateTimeOffset $DeliveryDate;

	/**
	 * The desired delivery date to set for the row.
	 */
	public ?DateTimeOffset $DesiredDeliveryDate;

	/**
	 * The discount to set for the row.Calculated by MONITOR by default
	 */
	public ?float $Discount;

	/**
	 * Creates a free text row as a child of this row with the specified content if set.Max length is 0
	 */
	public string $SubRowContent;

	/**
	 * The Id of the order row you want to insert before. If null the row will be added last.            References CustomerOrderRows.Introduced in version 2.22
	 */
	public ?int $InsertRowBeforeId;

	/**
	 * The Id of the order row you want to insert after. If null the row will be added last.            References CustomerOrderRows.Introduced in version 2.22
	 */
	public ?int $InsertRowAfterId;

	/**
	 * The dock to set for the row.Max length is 35Introduced in version 2.31
	 */
	public string $Dock;

	/**
	 * The dock name to set for the row.Max length is 80Introduced in version 2.31
	 */
	public string $DockName;

	/**
	 * The storage to set for the row.Max length is 35Introduced in version 2.31
	 */
	public string $Storage;

	/**
	 * The kanban number to set for the row.Max length is 80Introduced in version 2.31
	 */
	public string $KanbanNumber;

	/**
	 * The weight per unit of the row.Introduced in version 2.36
	 */
	public ?float $WeightPerUnit;

	/**
	 * The text type to set for the row.None: 0Quote: 1Acknowledgement: 2DeliveryNote: 4Invoice: 8Calculated by MONITOR by defaultIntroduced in version 2.35.3
	 */
	public ?int $ShowFreeTextIn;

	/**
	 * The customer commitment level of the row.FixedOrder: 1Manufacture: 2BuyMaterial: 3Forecast: 4FixedOrder (1) by defaultIntroduced in version 2.37
	 */
	public ?int $CustomerCommitmentLevel;

	/**
	 * Specifies the sub rows of type free text to add to this row.None by defaultIntroduced in version 2.37
	 */
	public array $SubRows = array();

	/**
	 * The business key identifier of the unit to set for the row.            References UnitsDefault unit on the part when the row type is part(1) by defaultIntroduced in version 2.39
	 */
	public ?int $UnitId;

	/**
	 * The standard price for the customer order row.Standard price of part when a part is present by defaultIntroduced in version 2.40
	 */
	public NotNullDecimalInput $StandardPrice;

	/**
	 * The goods label to set for the customer order row.Max length is 80Introduced in version 2.45
	 */
	public string $RowsGoodsLabel;

	/**
	 * The delivery reference number to set for the customer order row.Max length is 80Introduced in version 2.45
	 */
	public string $ReferenceNumberDelivery;

	/**
	 * The manufacturing reference number to set for the customer order row.Max length is 80Introduced in version 2.45
	 */
	public string $ReferenceNumberManufacturing;

	/**
	 * The alternate preparation code to set for the row.Max length is 200Introduced in version 2.46
	 */
	public string $AlternatePreparationCode;

	/**
	 * If the customer order row is to be included in payment plan.Calculated by MONITOR by defaultIntroduced in version 2.48
	 */
	public ?bool $IsIncludedInPaymentPlan;

}