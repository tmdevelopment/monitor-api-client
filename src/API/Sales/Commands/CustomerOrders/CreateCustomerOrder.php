<?php

namespace Monitor\API\Sales\Commands\CustomerOrders;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.CustomerOrders.CreateCustomerOrder.html
 */
class CreateCustomerOrder extends Base{

	/**
	 * The business key identifier of the customer to create a customerorder for.References Customers
	 */
	public string $CustomerId;

	/**
	 * The order number to set on the new customer order.Generated from number series and order type by default
	 * @maxlength 15
	 */
	public string $OrderNumber;

	/**
	 * The business key identifier of the order type to set for the newcustomer order.References CustomerOrderTypesLast used order type by default
	 */
	public string $OrderTypeId;

	/**
	 * The business key identifier of the warehouse to create the customerorder for.References WarehousesCurrent warehouse by default
	 */
	public string $WarehouseId;

	/**
	 * The business key identifier of the project to associate the customer order with.References ProjectsIntroduced in version 2.40
	 */
	public string $ProjectId;

	/**
	 * Specifies the customer order rows to add to the new customer order.None by default
	 */
	public array $Rows = array();

	/**
	 * Specifies if the customer order is a stock order.Introduced in version 2.41
	 */
	public bool $IsStockOrder;

}