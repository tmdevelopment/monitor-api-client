<?php

namespace Monitor\API\Sales\Commands\Customers;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.Customers.CreateCustomer.html
 */
class CreateCustomer extends Base{

	/**
	 * The name to set for the new customer
	 * MandatoryMax length is 50
	 * @required
	 */
	public string $Name;

	/**
	 * The human readable identifier to set for the new customer
	 * Generated from number series by defaultMax length is 10Unique
	 */
	public string $Code;

}