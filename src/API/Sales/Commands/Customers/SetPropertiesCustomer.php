<?php

namespace Monitor\API\Sales\Commands\Customers;

use Monitor\API\Infrastructure\BooleanInput;
use Monitor\API\Infrastructure\DateTimeInput;
use Monitor\API\Infrastructure\DecimalInput;
use Monitor\API\Infrastructure\IntegerInput;
use Monitor\API\Infrastructure\LongInput;
use Monitor\API\Infrastructure\NotNullBooleanInput;
use Monitor\API\Infrastructure\NotNullIntegerInput;
use Monitor\API\Infrastructure\NotNullLongInput;
use Monitor\API\Infrastructure\StringInput;
use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.Customers.SetPropertiesCustomer.html
 */
class SetPropertiesCustomer extends Base
{

	/**
	 * The business key identifier of the customer to update
	 * References CustomersMandatory
	 * @required
	 */
	public string $CustomerId;

	/**
	 * The name of the customer
	 * 
	 */
	public StringInput $Name;

	/**
	 * The human readable identifier of the customer
	 * 
	 */
	public StringInput $Code;

	/**
	 * The alternative name of the customer
	 * 
	 */
	public StringInput $AlternativeName;

	/**
	 * The corporation identification number of the customer
	 * 
	 */
	public StringInput $CorporationIdentificationNumber;

	/**
	 * The personal number of the customer
	 * 
	 */
	public StringInput $PersonalNumber;

	/**
	 * Specifies if the customer is a private person
	 * 
	 */
	public BooleanInput $IsPrivateCustomer;

	/**
	 * The VAT number of the customer
	 * 
	 */
	public StringInput $VatNumber;

	/**
	 * The business key identifier of the customer's account manager
	 *             References Persons
	 */
	public LongInput $AccountManagerId;

	/**
	 * The business key identifier of the customer's actualization probability
	 *             References Probabilities
	 */
	public LongInput $ActualCustomerProbabilityId;

	/**
	 * The alias code of the customer
	 * 
	 */
	public StringInput $Alias;

	/**
	 * The alloy cost associated with the customer
	 * 
	 */
	public IntegerInput $AlloyCost;

	/**
	 * Specifies if bank charge should be applied for the customer
	 * 
	 */
	public BooleanInput $ApplyBankCharge;

	/**
	 * Specifies if invoicing charge should be applied for the customer
	 * 
	 */
	public BooleanInput $ApplyInvoicingCharge;

	/**
	 * Specifies if reminder charge should be applied for the customer
	 * 
	 */
	public BooleanInput $ApplyReminderCharge;

	/**
	 * Specifies if data file should be attached with orders for the customer
	 * 
	 */
	public BooleanInput $AttachDatafileWithOrder;

	/**
	 * Specifies if data file should be attached with invoices for the customer
	 * 
	 */
	public BooleanInput $AttachDatafileWithInvoice;

	/**
	 * Specifies if data file should be attached with cases for the customer
	 * 
	 */
	public BooleanInput $AttachDatafileWithCase;

	/**
	 * Specifies if data file should be attached with parts for the customer
	 * 
	 */
	public BooleanInput $AttachDatafileWithPart;

	/**
	 * Specifies if data file should be attached with dispatch advice for the customer
	 * 
	 */
	public BooleanInput $AttachDatafileWithDispatchAdvice;

	/**
	 * The bank charge amount of the customer
	 * 
	 */
	public DecimalInput $BankChargeAmount;

	/**
	 * The bank giro number of the customer
	 * 
	 */
	public StringInput $BankGiroNo;

	/**
	 * The category string of the customer
	 * 
	 */
	public StringInput $CategoryString;

	/**
	 * The business key identifier of the configuration price list that the customer belongs to
	 *             References PriceLists
	 */
	public LongInput $ConfigurationPriceListId;

	/**
	 * The credit limit of the customer
	 * 
	 */
	public DecimalInput $CreditLimit;

	/**
	 * The business key identifier of the customer's currency
	 *             References 
	 */
	public LongInput $CurrencyId;

	/**
	 * The business key identifier of the district that the customerbelongs to
	 *             References CustomerDistricts
	 */
	public LongInput $DistrictId;

	/**
	 * The date/time when transition to actual contact was made for the customer
	 * 
	 */
	public DateTimeInput $DateForTransitionToActualContact;

	/**
	 * The general discount of the customer
	 * 
	 */
	public DecimalInput $Discount;

	/**
	 * The business key identifier of the discount category that applies for thecustomer
	 *             References DiscountCategories
	 */
	public LongInput $DiscountCategoryId;

	/**
	 * The preferred order printout method of the customer
	 * 
	 */
	public IntegerInput $DefaultOrderPrintoutVia;

	/**
	 * The delivery instruction of the customer
	 * 
	 */
	public StringInput $DeliveryInstruction;

	/**
	 * Specifies if invoices should be pending by default for the customer
	 * 
	 */
	public BooleanInput $DefaultInvoiceStatusPending;

	/**
	 * The EDI code of the customer
	 * 
	 */
	public StringInput $EdiCode;

	/**
	 * Specifies if factoring applies for the customer
	 * 
	 */
	public BooleanInput $Factoring;

	/**
	 * The grace period of the customer
	 * 
	 */
	public IntegerInput $GracePeriod;

	/**
	 * Specifies the interest type of the customer
	 * InterestInvoice: 0NextRegularInvoice: 1None: 2
	 */
	public IntegerInput $InterestType;

	/**
	 * Specifies if the customer is a stray customer
	 * 
	 */
	public BooleanInput $IsCasualCustomer;

	/**
	 * Specifies the interest invoice days for the customer
	 * 
	 */
	public IntegerInput $InterestInvoiceDays;

	/**
	 * The insurance cost specified for the customer
	 * 
	 */
	public DecimalInput $InsuranceCost;

	/**
	 * The business key identifier of the invoice customer of the customer
	 *             References Customers
	 */
	public LongInput $InvoiceCustomerId;

	/**
	 * The customer's preferred printout method for invoices
	 * Printer: 0Email: 1
	 */
	public IntegerInput $InvoicePrintoutMethod;

	/**
	 * Specifies when invoices should be printed for the customer
	 * AfterDeliveryReport: 0OnDeliveryReportPreliminary: 1OnDeliveryReportDefinite: 2
	 */
	public IntegerInput $InvoicePrintoutOccasion;

	/**
	 * The business key identifier of the customer's preferred language
	 *             References LanguageCodes
	 */
	public LongInput $LanguageId;

	/**
	 * The late payment fee of the customer
	 * 
	 */
	public DecimalInput $LatePaymentFee;

	/**
	 * The number of invoice copies that should be produced for the customer
	 * 
	 */
	public IntegerInput $NumberOfInvoiceCopies;

	/**
	 * The supplier code assigned to us by the customer
	 * 
	 */
	public StringInput $OurCodeByYou;

	/**
	 * The customer's pallet registration number
	 * 
	 */
	public StringInput $PalletRegistrationNo;

	/**
	 * The business key identifier of the customer's payment term
	 *             References PaymentTerms
	 */
	public LongInput $PaymentTermId;

	/**
	 * The PlusGiro number of the customer
	 * 
	 */
	public StringInput $PlusGiroNo;

	/**
	 * The business key identifier of the customer's price list
	 *             References PriceLists
	 */
	public LongInput $PriceListId;

	/**
	 * The priority of the customer
	 * 
	 */
	public IntegerInput $Priority;

	/**
	 * Specifies when proforma invoices should be printed for the customer
	 * AfterDeliveryReport: 0OnDeliveryReport: 1
	 */
	public IntegerInput $ProformaInvoicePrintoutOccasion;

	/**
	 * The business key identifier of the customer's reseller
	 *             References Persons
	 */
	public LongInput $ResellerId;

	/**
	 * Specifies if round off applies for the customer
	 * 
	 */
	public BooleanInput $RoundOff;

	/**
	 * The business key identifier of the customer's seller
	 *             References Persons
	 */
	public LongInput $SellerId;

	/**
	 * The business key identifier of the customer's service agreement
	 *             References ServiceAgreement
	 */
	public LongInput $ServiceAgreementId;

	/**
	 * Specifies which documents should display statistical goods codes and country of originfor the customer
	 * None: 0Quote: 1Acknowledgement: 2DeliveryNote: 4Invoice: 8
	 */
	public IntegerInput $ShowPartStatisticalGoodsCodeAndCountryOfOriginInForms;

	/**
	 * Specifies which documents should show part weight for the customer
	 * None: 0Quote: 1Acknowledgement: 2DeliveryNote: 4Invoice: 8
	 */
	public IntegerInput $ShowPartWeightInForms;

	/**
	 * The business key identifier of the customer's status
	 *             References CustomerStatuses
	 */
	public LongInput $StatusId;

	/**
	 * The tolerance for late delivery to set in days
	 * 
	 */
	public IntegerInput $ToleranceForLateDelivery;

	/**
	 * The tolerance for early delivery to set in days
	 * 
	 */
	public IntegerInput $ToleranceForEarlyDelivery;

	/**
	 * The business key identifier of the customer's type
	 *             References CustomerTypes
	 */
	public LongInput $TypeId;

	/**
	 * The URL of the customer
	 * 
	 */
	public StringInput $Url;

	/**
	 * Specifies if comprehensive invoices should be used for the customer
	 * 
	 */
	public BooleanInput $UseComprehensiveInvoices;

	/**
	 * Specifies if comprehensive invoices should be used for the customer and if so, what they should be grouped on
	 * None: 0Customer: 1Order: 2DeliveryNote: 3
	 */
	public IntegerInput $ComprehensiveInvoiceGroupingMode;

	/**
	 * Specifies if comprehensive delivery notes should be used for the customer
	 * 
	 */
	public BooleanInput $UseDeliveryNoteCollectionFormReport;

	/**
	 * Specifies if forward rate should be used for the customer
	 * 
	 */
	public BooleanInput $UseForwardRate;

	/**
	 * Specifies if VAT number should be taken via the invoice customer
	 * 
	 */
	public BooleanInput $UseInvoiceCustomersVatNumber;

	/**
	 * Specifies if reminders should be enabled for the customer
	 * 
	 */
	public BooleanInput $UseReminder;

	/**
	 * The business key identifier of the customer's VAT rate
	 *             References VatRates
	 */
	public LongInput $VatRateId;

	/**
	 * The business key identifier of the customer's validity time
	 *             References ValidityTimes
	 */
	public LongInput $ValidityTimeId;

	/**
	 * The business key identifier of the customer's payment plan template
	 *             References Introduced in version 2.48
	 */
	public LongInput $PaymentPlanTemplateId;

	/**
	 * The time zone of the customer
	 *  Specified as a time zone identifier (e.g. "W. Europe Standard Time").Max length is 80
	 */
	public StringInput $TimeZone;

	/**
	 * The decimal symbol of the customer
	 * Max length is 1
	 */
	public StringInput $DecimalSymbol;

	/**
	 * The date display format of the customer
	 * YYYYMMDD_Dash: 1YYMMDD_Dash: 2DDMMYYYY_Dash: 3DDMMYY_Dash: 4YYYYMMDD_Slash: 5YYMMDD_Slash: 6MMDDYYYY_Slash: 7MMDDYY_Slash: 8MDYY_Slash: 9DDMMYYYY_Slash: 10DDMMYY_Slash: 11DMMYY_Slash: 12YYYYMMDD_Dot: 13DDMMYYYY_Dot: 14DDMMYY_Dot: 15DDMYY_Dot: 16DMMYYYY_Dot: 17DMMYY_Dot: 18DMYYYY_Dot: 19DMYY_Dot: 20YYWWD: 21
	 */
	public NotNullIntegerInput $DateDisplayFormat;

	/**
	 * Specifies if the customer uses e-invoicing
	 * 
	 */
	public NotNullBooleanInput $UseEInvoicing;

	/**
	 * The e-invoice electronic address identifier of the customer
	 * Max length is 6
	 */
	public StringInput $EInvoiceElectronicAddressIdentifier;

	/**
	 * The e-invoice id of the customer
	 * Max length is 30
	 */
	public StringInput $EInvoiceId;

	/**
	 * Specifies if the customer should apply preliminary picking list
	 * 
	 */
	public NotNullBooleanInput $PreliminaryPickingList;

	/**
	 * Specifies if the customer is included in delivery analysis
	 * 
	 */
	public NotNullBooleanInput $IncludeInDeliveryAnalysis;

	/**
	 * The business key identifier of the customer's active delivery address account group
	 *             References CustomerAccountGroupsReferences CustomerAccountGroups
	 */
	public NotNullLongInput $CustomerAccountGroupId;

	/**
	 * The business key identifier of the VAT group that the delivery address belongs to
	 * References VatGroups.References VatGroups
	 */
	public NotNullLongInput $VatGroupId;

	/**
	 * The business key identifier of the customer's currency exchange type
	 * References CurrencyExchangeTypes
	 */
	public NotNullLongInput $CurrencyExchangeTypeId;
}