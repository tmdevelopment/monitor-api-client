<?php

namespace Monitor\API\Sales\Commands\Customers;

use Monitor\API\Common\Commands\Shared\SetAddressCountry;
use Monitor\API\Infrastructure\LongInput;
use Monitor\API\Infrastructure\NotNullLongInput;
use Monitor\API\Infrastructure\StringInput;
use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.Customers.UpdateCustomerMailingAddress.html
 */
class UpdateCustomerMailingAddress extends Base{

	/**
	 * The business key identity of the customer to update mailing addressfor
	 * References CustomersMandatory
	 * @required
	 */
	public string $CustomerId;

	/**
	 * The addressee of the address
	 * 
	 */
	public StringInput $Addressee;

	/**
	 * The first field of the address
	 * 
	 */
	public StringInput $Field1;

	/**
	 * The second field of the address
	 * 
	 */
	public StringInput $Field2;

	/**
	 * The third field of the address
	 * 
	 */
	public StringInput $Field3;

	/**
	 * The fourth field of the address
	 * 
	 */
	public StringInput $Field4;

	/**
	 * The fifth field of the address
	 * 
	 */
	public StringInput $Field5;

	/**
	 * The locality of the address
	 * 
	 */
	public StringInput $Locality;

	/**
	 * The region of the address
	 * 
	 */
	public StringInput $Region;

	/**
	 * The postal code of the address
	 * 
	 */
	public StringInput $PostalCode;

	/**
	 * The business key identifier of the language code
	 * References LanguageCodes.
	 */
	public NotNullLongInput $LanguageId;

	/**
	 * The business key identifier of the form report translation group
	 * References FormReportTranslationGroup.
	 */
	public LongInput $FormReportTranslationGroupId;

	/**
	 * The business key identifier of the postal code
	 * References PostalCode.
	 */
	public LongInput $PostalCodeId;

	/**
	 * Sets the country of the delivery address
	 * 
	 */
	public SetAddressCountry $SetAddressCountry;

}