<?php

namespace Monitor\API\Sales\Commands\Customers;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.Customers.AddCommunicationAddressCustomer.html
 */
class AddCommunicationAddressCustomer extends Base
{

	/**
	 * The business key identifier of the customer to create a communication address for
	 * References CustomersMandatory
	 * @required
	 */
	public string $CustomerId;

	/**
	 * The value of the communication address
	 * MandatoryNot nullableMax length is 80
	 * @required
	 */
	public string $CommunicationAddressValue;

	/**
	 * The type of information stored within this particular communication address instance
	 * Phone: 0Fax: 1Email: 2CellPhone: 3Phone (0) by default
	 */
	public ?int $Type;

	/**
	 * Any remarks regarding the communication address
	 * Max length is 80
	 */
	public string $Remarks;

	/**
	 * What the communication address should be recipient of
	 * None: 0Invoice: 1Reminder: 2OrderConfirmation: 4DispatchAdvice: 8ShippingAgentAdvice: 16Quote: 512ShippingAgentSmsAdvice: 1024Proforma: 131072ProjectReportExternal: 262144CustomerDeliveryNote: 1048576TraceabilityProtocol: 2097152None (0) by default
	 */
	public ?int $RecipientOf;

	/**
	 * The contact name of  the communication address
	 * Max length is 80
	 */
	public string $ContactName;
}
