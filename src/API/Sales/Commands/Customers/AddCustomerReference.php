<?php

namespace Monitor\API\Sales\Commands\Customers;

use Monitor\API\Infrastructure\StringInput;
use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.Customers.AddCustomerReference.html
 */
class AddCustomerReference extends Base
{

    /**
     * The business key identifier of the customer to add a referenceto
     * References CustomersMandatory
     * @required
     */
    public string $CustomerId;

    /**
     * The name of the business contact reference
     * @maxlength 40
     */
    public StringInput $Name;

    /**
     * The first extra information field of the business contact reference
     * 
     */
    public StringInput $ExtraInformation1;

    /**
     * The second extra information field of the business contact reference
     * 
     */
    public StringInput $ExtraInformation2;

    /**
     * The third extra information field of the business contact reference
     * 
     */
    public StringInput $ExtraInformation3;

    /**
     * The category of the business contact reference
     * 
     */
    public StringInput $Category;

    /**
     * Any extra notes regarding the business contact reference
     * 
     */
    public StringInput $Note;

    /**
     * The value of the communication address
     * 
     */
    public StringInput $CellPhoneNumber;

    /**
     * The value of the communication address
     * 
     */
    public StringInput $PhoneNumber;

    /**
     * The value of the communication address
     * 
     */
    public StringInput $EmailAddress;

    /**
     * The value of the communication address
     * 
     */
    public StringInput $FaxNumber;
}
