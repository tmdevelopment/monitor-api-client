<?php

namespace Monitor\API\Sales\Commands\Customers;

use Monitor\API\Common\Commands\Shared\SetAddressCountry;
use Monitor\API\Infrastructure\IntegerInput;
use Monitor\API\Infrastructure\LongInput;
use Monitor\API\Infrastructure\NotNullLongInput;
use Monitor\API\Infrastructure\StringInput;
use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.Customers.AddCustomerDeliveryAddress.html
 */
class AddCustomerDeliveryAddress extends Base
{

    /**
     * The business key identifier of customer to add a delivery addressto
     * References CustomersMandatory
     * @required
     */
    public string $CustomerId;

    /**
     * Specifies if the delivery address should be marked as the defaultfor the customer
     * Mandatory
     * @required
     */
    public bool $SetAsDefault;

    /**
     * The addressee of the address
     * 
     */
    public StringInput $Addressee;

    /**
     * The first field of the address Street/House/Apartment
     * 
     */
    public StringInput $Field1;

    /**
     * The second field of the address, City/Town/Village
     * 
     */
    public StringInput $Field2;

    /**
     * The third field of the address County
     * 
     */
    public StringInput $Field3;

    /**
     * The fourth field of the address
     * 
     */
    public StringInput $Field4;

    /**
     * The fifth field of the address Zip code
     * 
     */
    public StringInput $Field5;

    /**
     * The locality of the address, Shows after ZIP in Monitor
     * 
     */
    public StringInput $Locality;

    /**
     * The region of the address
     * 
     */
    public StringInput $Region;

    /**
     * The postal code of the address
     * 
     */
    public StringInput $PostalCode;

    /**
     * The business key identifier of the language code
     * References LanguageCodes.
     */
    public NotNullLongInput $LanguageId;

    /**
     * The business key identifier of the form report translation group
     * References FormReportTranslationGroup.
     */
    public LongInput $FormReportTranslationGroupId;

    /**
     * The business key identifier of the postal code
     * References PostalCode.
     */
    public LongInput $PostalCodeId;

    /**
     * Sets the country of the delivery address
     * 
     */
    public SetAddressCountry $SetAddressCountry;

    /**
     * References BusinessContactReferences
     * Introduced in version 2.27
     */
    public LongInput $ConsigneeReferenceId;

    /**
     * The consignee reference name of the delivery address
     * Introduced in version 2.27
     */
    public StringInput $ConsigneeReferenceName;

    /**
     * Introduced in version 2
     * 27
     */
    public StringInput $Destination;

    /**
     * The instruction of delivery for the delivery address
     * Introduced in version 2.27
     */
    public StringInput $DeliveryInstruction;

    /**
     * The business key identifier of the VatRate of the delivery address
     * References VatRates.Introduced in version 2.27
     */
    public LongInput $VatRateId;

    /**
     * The business key identifier of the customer account group of the delivery address
     * References CustomerAccountGroups.Introduced in version 2.27
     */
    public LongInput $CustomerAccountGroupId;

    /**
     * The business key identifier of the supplier account group of the delivery address
     * References SupplierAccountGroup.Introduced in version 2.27
     */
    public LongInput $SupplierAccountGroupId;

    /**
     * The business key identifier of the VAT group that the delivery address belongs to
     * References VatGroups.Introduced in version 2.27
     */
    public LongInput $VatGroupId;

    /**
     * The delivery address does have some properties that can be different depending on the current warehouse
     * Setting this property ensures that the setting is applied to only that warehouse. If the value is null the values are applied to the setting for all warehouses.Affected properties are: * DeliveryTerm * DeliveryMethod* DestinationForDeliveryTerm* TransportTime* DeliveryWeekdaysReferences WarehousesIntroduced in version 2.27
     */
    public ?int $WarehouseId;

    /**
     * The business key identifier of the customer order's delivery method
     *             References DeliveryMethodsIntroduced in version 2.27
     */
    public LongInput $DeliveryMethodId;

    /**
     * The business key identifier of the customer order's delivery term
     *             References DeliveryTermsIntroduced in version 2.27
     */
    public LongInput $DeliveryTermId;

    /**
     * The destination for the delivery term
     * Introduced in version 2.27
     */
    public StringInput $DestinationForDeliveryTerm;

    /**
     * The transport time of the customer order in workdays
     * Introduced in version 2.27
     */
    public IntegerInput $TransportTime;

    /**
     * Specifies which days delivery can be made to the delivery address
     * Introduced in version 2.27
     */
    public IntegerInput $DeliveryWeekdays;
}
