<?php

namespace Monitor\API\Sales\Commands\Quotes;

use Monitor\Base;
use Monitor\DateTimeOffset;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.Quotes.AddQuoteRow.html
 */
class AddQuoteRow extends Base{

	/**
	 * The business key identifier of the quote to add a row to
	 *             References QuotesMandatory
	 * @required
	 */
	public int $QuoteId;

	/**
	 * The business key identifier of the part to set on the row
	 *  Only applies to part andadditional rows.            References Parts
	 */
	public ?int $PartId;

	/**
	 * The session id of the part configuration to apply for the quote row
	 * Introduced in version 2.33
	 */
	public ?string $PartConfigurationSessionId;

	/**
	 * The additional part code to set for the row
	 * Max length is 20
	 */
	public string $AdditionalPartCode;

	/**
	 * The part description to set for the row
	 *  Only applies for part and additional rows.Max length is 80Same description as the part set for the row by default
	 */
	public string $PartDescription;

	/**
	 * The text row data to set for the row
	 *  Only applies to text rows.Max length is 0
	 */
	public string $TextRowData;

	/**
	 * The quantity to set for the row in the part's customer order unit
	 * Mandatory
	 * @required
	 */
	public float $OrderedQuantity;

	/**
	 * The price to set for the row in the quote currency
	 * Calculated by MONITOR by default
	 */
	public ?float $Price;

	/**
	 * The setup price to set for the row in the quote currency
	 * Calculated by MONITOR by default
	 */
	public ?float $SetupPrice;

	/**
	 * The business key identifier of the warehouse to set for the quote row
	 *             References WarehousesSame warehouse as the quote by default
	 */
	public ?int $WarehouseId;

	/**
	 * The row type to set for the quote row
	 * Part: 1Additional: 2Sum: 3FreeText: 4Mandatory
	 * @required
	 */
	public int $OrderRowType;

	/**
	 * The delivery date to set for the row
	 * 
	 */
	public ?DateTimeOffset $DeliveryDate;

	/**
	 * Sets the discount of the row
	 * Calculated by MONITOR by default
	 */
	public ?float $Discount;

	/**
	 * Creates a new free text sub row of this row with the specified content as text rowdata
	 * Max length is 0
	 */
	public string $SubRowContent;

	/**
	 * The Id of the order row you want to insert before
	 *  If null the row will be added last.            References QuoteRows.Introduced in version 2.22
	 */
	public ?int $InsertRowBeforeId;

	/**
	 * The Id of the order row you want to insert after
	 *  If null the row will be added last.            References QuoteRows.Introduced in version 2.22
	 */
	public ?int $InsertRowAfterId;

	/**
	 * The weight per unit of the row
	 * Introduced in version 2.36
	 */
	public ?float $WeightPerUnit;

	/**
	 * The goods label of the rowExpandableIntroduced in version 2
	 * 40
	 */
	public string $RowsGoodsLabel;

	/**
	 * The alternate preparation code of the rowExpandableIntroduced in version 2
	 * 40
	 */
	public string $AlternatePreparationCode;

	/**
	 * The business key identifier of the unit to set for the row
	 * References UnitsIntroduced in version 2.48Defaults to the default unit usage for customer order on the part by default
	 */
	public ?int $UnitId;

}