<?php

namespace Monitor\API\Sales\Commands\Quotes;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.Quotes.CreateQuote.html
 */
class CreateQuote extends Base{

	/**
	 * The business key identifier of the customer to create a quote for
	 * References CustomersMandatory
	 * @required
	 */
	public string $CustomerId;

	/**
	 * The quote number to set for the new quote
	 * Generated from number series and quote type by default
	 */
	public string $QuoteNumber;

	/**
	 * The business key identifier of the quote type to apply to the new quote
	 * References QuoteTypesLast used quote type by default
	 */
	public ?string $QuoteTypeId;

	/**
	 * The rows to add to the new quote
	 * None by default
     * @var AddQuoteRow[]
	 */
	public array $Rows;

}