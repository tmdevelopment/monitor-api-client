<?php

namespace Monitor\API\Common;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Comment.html
 */
class Comment extends Base{

	/**
	 * CSharp type is Int64
	 * The business key identifier of the comment.
	 */
	public string $Id;

	/**
	 * The comment text with HTML formatting.
	 */
	public string $Text;

	/**
	 * The comment text without HTML formatting. In MONITOR this is used exclusivelyfor indexing purposes and is not shown anywhere in the system.
	 */
	public string $RawText;

	/**
	 * Specifies if the comment has files linked to it.
	 */
	public bool $HasFileLinks;

	/**
	 * A collection of all file links that belong to the comment.Expandable
	 */
	public ?array $FileLinks;

}