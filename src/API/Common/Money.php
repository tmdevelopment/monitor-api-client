<?php

namespace Monitor\API\Common;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Money.html
 */
class Money extends Base{

	/**
	 * The money amount.
	 */
	public float $Amount;

	/**
	 * The business key identifier of the currency that the amount is specifiedin.References Currencies
	 */
	public ?int $CurrencyId;

}