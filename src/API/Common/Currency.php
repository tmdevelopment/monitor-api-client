<?php

namespace Monitor\API\Common;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Currency.html
 */
class Currency extends Base{

	/**
	 * CSharp type is Int64
	 * The business key identifier of the currency
	 * 
	 */
	public string $Id;

	/**
	 * The human readable identifier of the currency
	 * Max length is 6Not nullableUnique
	 */
	public string $Code;

	/**
	 * The translated description of the currency
	 * 
	 */
	public string $Description;

	/**
	 * The rate factor of the currency
	 * 
	 */
	public int $RateFactor;

	/**
	 * The default sales exchange rate
	 * 
	 */
	public float $DefaultSalesExchangeRate;

	/**
	 * The default purchase exchange rate
	 * 
	 */
	public float $DefaultPurchaseExchangeRate;

	/**
	 * All available exchange rates for the currency
	 * Expandable
	 * @var CurrencyExchangeRates[] $ExchangeRates
	 */
	public array $ExchangeRates;

	/**
	 * All previous exchange rates for the currencyIntroduced in version 2
	 * 26Expandable
	 * $var CurrencyExchangeRateChangeLogs[] $ExchangeRatesChangeLog 
	 */
	public array $ExchangeRatesChangeLog;

}