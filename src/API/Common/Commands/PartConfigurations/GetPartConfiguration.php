<?php

namespace Monitor\API\Common\Commands\PartConfigurations;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.GetPartConfiguration.html
 */
class GetPartConfiguration extends Base{

	/**
	 * The session id of the part configuration to get a snapshot of.Mandatory
	 * @required
	 */
	public string $SessionId;

}