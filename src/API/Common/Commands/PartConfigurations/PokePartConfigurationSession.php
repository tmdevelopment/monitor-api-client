<?php

namespace Monitor\API\Common\Commands\PartConfigurations;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.PokePartConfigurationSession.html
 */
class PokePartConfigurationSession extends Base{

	/**
	 * The session id of the part configuration to poke.Mandatory
	 * @required
	 */
	public string $SessionId;

	/**
	 * The expiry time to set for this and future pokes and updates.Current session expiry time by default
	 */
	public string $ExpiryTime; 

}