<?php
namespace Monitor\API\Common\Commands\PartConfigurations;

class CreateForCustomerOrder{
	/**
	 * @required
	 */
	public string $PartId;
	/**
	 * @required
	 */
	public float $Quantity;
	/**
	 * @required
	 */
	public string $CurrencyId;
	/**
	 * @required
	 */
	public string $CustomerId;
	/**
	 * @required
	 */
	public string $CustomerOrderTypeId;
	
	public ?int $SourcePartConfigurationId;
	public bool $UseSourcePartConfigurationSnapshot;

}