<?php

namespace Monitor\API\Common\Commands\PartConfigurations;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.PartConfigurationSessionExists.html
 */
class PartConfigurationSessionExists extends Base{

	/**
	 * The session id to look for.Mandatory
	 * @required
	 */
	public string $SessionId;

}