<?php

namespace Monitor\API\Common\Commands\PartConfigurations;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Sales.Commands.PartConfigurations.CreatePartConfigurationForQuote.html
 */
class CreatePartConfigurationForQuote extends Base
{

    /**
     * The business key identifier of the part to create a part configuration session for
     * References PartsMandatory
     * @required
     */
    public string $PartId;

    /**
     * The quantity to configure
     * Mandatory
     * @required
     */
    public float $Quantity;

    /**
     * The business key identifier of the currency to use when calculating pricesfor the part configuration session
     * References CurrenciesMandatory
     * @required
     */
    public string $CurrencyId;

    /**
     * The business key identifier of the customer to use for the part configuration session
     * References CustomersMandatory
     * @required
     */
    public string $CustomerId;

    /**
     * The business key identifier of the quote type to use for the part configuration session
     * References QuoteTypesMandatory
     * @required
     */
    public string $QuoteTypeId;

    /**
     * The business key identifier of the part configuration preset to use for the part configuration session.
     * References PartConfigurationPresets
     * Introduced in version 23.7
     * @var ?string $PresetId
     */
    public ?string $PresetId;
}
