<?php

namespace Monitor\API\Common\Commands\PartConfigurations;

class UpdatePartConfiguration{
	/**
	 * @required
	 */
	public string $SessionId;
	public array $Instructions = array();

}