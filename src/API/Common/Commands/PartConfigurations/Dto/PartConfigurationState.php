<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

use Monitor\API\Common\Comment;
use Monitor\API\Common\Money;
use Monitor\Base;
use Monitor\DateTimeOffset;
use Exception;
use Monitor\API\Common\Commands\PartConfigurations\UpdatePartConfiguration;
use Monitor\PartConfigurationUpdateType;
use Monitor\UnserializeCast;
use Monitor\VariableType;
use TypeError;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.PartConfigurationState.html
 */
class PartConfigurationState extends Base
{
	use SelectionGroupIterator;
	use UnserializeCast;
	/**
	 * The session id of the part configuration.
	 */
	public string $SessionId;

	/**
	 * When the session expires on the server.
	 */
	public DateTimeOffset $ExpiresAt;

	/**
	 * The business key identifier of the customer that the part configuration wascalculated for.References Customers
	 */
	public string $CustomerId;

	/**
	 * The part configuration comment.
	 */
	public ?Comment $Comment;

	/**
	 * CSharp type is Int64
	 * The business key identifier of the part configuration template snapshot thatthe part configuration is based on.References PartConfigurationTemplateSnapshot
	 */
	public string $PartConfigurationTemplateSnapshotId;

	/**
	 * The business key identifier of the part configuration template that the partconfiguration is based on.References PartConfigurationTemplate
	 */
	public string $PartConfigurationTemplateId;

	/**
	 * The template version of the part configuration.
	 */
	public string $PartConfigurationTemplateVersion;

	/**
	 * The business key identifier of the part configuration.References PartConfiguration
	 */
	public string $PartConfigurationId;

	/**
	 * Specifies if the part configuration is valid.
	 */
	public bool $IsValid;

	/**
	 * The resulting alternative preparation code of the part configuration.
	 */
	public string $AlternativePreparationCode;

	/**
	 * The calculated discount percentage of the part configuration.
	 */
	public float $DiscountPercentage;

	/**
	 * Specifies if the discount of the part configuration is locked.
	 */
	public bool $LockedDiscount;

	/**
	 * Specifies if the unit price of the part configuration is locked.
	 */
	public bool $LockedUnitPrice;

	/**
	 * The business key identifier of the part that the part configuration belongsto.References Parts
	 */
	public string $PartId;

	/**
	 * The price formula factor of the part configuration.
	 */
	public float $PriceFormulaFactor;

	/**
	 * The configured quantity of the part configuration.
	 */
	public float $Quantity;

	/**
	 * The calculated standard price of the part configuration.
	 */
	public Money $StandardPrice;

	/**
	 * The calculated unit price of the part configuration.
	 */
	public Money $UnitPrice;

	/**
	 * The calculated unit price of the part configuration in the companycurrency.
	 */
	public Money $UnitPriceInCompanyCurrency;

	/**
	 * The calculated weight per unit of the part configuration.
	 */
	public ?float $WeightPerUnit;

	/**
	 * The sections of the part configuration.
	 * @var \Monitor\API\Common\Commands\PartConfigurations\Dto\SectionState[] $Sections
	 */
	public array $Sections;

	/**
	 * Custom field, holding validation errors in flat array.
	 */
	public array $ValidationErrors;

	/**
	 * non monitor fields, added for convenience
	 */
	public ?float $TotalPrice;
	public string $PartNumber;

	private static function getVariableValidationResults(array $Variables): array
	{
		$result = array();
		foreach ($Variables as $Variable) {
			if (!empty($Variable->ValidationResults)) {
				$result[] = $Variable;
			}
		}
		return $result;
	}
	private static function getSelectionGroupValidationResults(array $SelectionGroups, array &$result = array()): array
	{
		foreach ($SelectionGroups as $SelectionGroup) {
			if (!empty($SelectionGroup->ValidationResults)) {
				$result[] = $SelectionGroup;
			}
			if (!empty($SelectionGroup->Rows)) {
				foreach ($SelectionGroup->Rows as $Row) {
					if (!empty($Row->ValidationResults)) {
						$result[] = $Row;
					}
				}
			}
			self::getSelectionGroupValidationResults($SelectionGroup->SelectionGroups, $result);
		}
		return $result;
	}

	static function getVariablesAndSelectionGroupsWithValidationErrors(object $state): array
	{
		$result = array();
		foreach ($state->Sections as $section) {
			$result += self::getVariablesAndSelectionGroupsWithValidationErrors($section);
			$result += self::getVariableValidationResults($section->Variables);
			$result += self::getSelectionGroupValidationResults($section->SelectionGroups);
		}
		return $result;
	}

	static function getFirstRowSelectedUptateInformation($state, $sessionId)
	{

		if ($state->IsAvailable == false) {
			throw new Exception("Unhandled code path");
		}

		if (isset($state->Value)) {
			$update = new UpdatePartConfigurationInstruction();
			$update->Type = PartConfigurationUpdateType::Variable;
			$update->Variable = new VariableUpdate();
			$update->Variable->VariableId = $state->Id;
			$update->Variable->Value = new VariableValue();
			$update->Variable->Value->Type = $state->Value->Type;

			if ($state->Value->Type == VariableType::Numeric) {
				$update->Variable->Value->NumericValue = $state->DefaultValue->NumericValue;
			} else if ($state->Value->Type == VariableType::Boolean) {
				$update->Variable->Value->BooleanValue = $state->DefaultValue->BooleanValue;
			} else if ($state->Value->Type == VariableType::DateTime) {
				$update->Variable->Value->DateTimeValue = $state->DefaultValue->DateTimeValue;
			} else if ($state->Value->Type == VariableType::String) {
				$update->Variable->Value->StringValue = $state->DefaultValue->StringValue;
			}

			$updatePartConfiguration = new UpdatePartConfiguration();
			$updatePartConfiguration->SessionId = $sessionId;
			$updatePartConfiguration->Instructions[] = $update;
			return $updatePartConfiguration;
		} elseif (isset($state->Rows)) {

			$updatePartConfiguration = new UpdatePartConfiguration();
			$updatePartConfiguration->SessionId = $sessionId;

			$selectedFound = false;
			$firstSelectedId = null;

			foreach ($state->Rows as $row) {
				if ($row->IsAvailable && $row->IsSelected) {
					$firstSelectedId = $row->Id;
					break;
				}
			}

			foreach ($state->Rows as $row) {



				$update = new UpdatePartConfigurationInstruction();
				$update->Type = PartConfigurationUpdateType::SelectionGroupRow;
				$update->SelectionGroupRow = new SelectionGroupRowUpdate();

				$update->SelectionGroupRow->SelectionGroupRowId = intval($row->Id);
				//$update->SelectionGroupRow->Selected = $row->IsSelected;

				if ($row->IsAvailable && ($firstSelectedId == null || $row->Id == $firstSelectedId) && $selectedFound == false) {
					$update->SelectionGroupRow->Selected = true;
					$selectedFound = true;
				} else {
					$update->SelectionGroupRow->Selected = false;
				}
				$updatePartConfiguration->Instructions[] = $update;
			}
			return $updatePartConfiguration;
		}
		throw new Exception("Unhandled code path");
	}

	function GetPrice()
	{
		$this->TotalPrice = $this->UnitPrice->Amount;
		$this->TotalPrice += $this->GetSelectionGroupPrices($this);
		return $this->TotalPrice;
	}

	private function GetSelectionGroupPrices($SelectionGroupOrSection = null): float
	{
		$price = 0;
		if (isset($SelectionGroupOrSection->Sections)) {
			foreach ($SelectionGroupOrSection->Sections as $section) {
				$price += $this->GetSelectionGroupPrices($section);
			}
		}

		if (isset($SelectionGroupOrSection->SelectionGroups)) {
			foreach ($SelectionGroupOrSection->SelectionGroups as $SelectionGroup) {
				if ($SelectionGroup->IsAvailable == true && !empty($SelectionGroup->Rows)) {
					foreach ($SelectionGroup->Rows as $Row) {
						if ($Row->IsSelected && $Row->IsAvailable) {
							$price += $Row->UnitPrice->Amount * $Row->Quantity;
						}
					}
				}
				$price += $this->GetSelectionGroupPrices($SelectionGroup);
			}
		}
		return $price;
	}

	public static function clean(&$input, $inputKey = null): void
	{

		//if($inputKey === "SelectionComment"){
		//	$input = null; 
		//}

		if ($input == null) {
			return;
		}

		//cleanComments
		if (is_string($input) && $inputKey !== "Description") {
			if (self::isEmptyHtml($input)) {
				$input = null;
			}
		}
		if (is_object($input)) {
			foreach (get_object_vars($input) as $key => $var) {

				switch ($key) {
					case '$id':
					case 'ExtraFieldTemplateId':
					case 'AlternativePreparationCode':
					case 'BindPropertyType':
					case 'CloneId':
					case 'Instruction':
					case 'Comment':
					case 'RowIndex':
					case 'Formula':
					case 'PriceFormulaText':
					case 'QuantityFormula':
					case 'SelectionComment':
						unset($input->$key);
						break;
					case "VariableType":
						switch ($input->$key) {
							case VariableType::String:
								unset($input->DefaultValue->NumericValue);
								unset($input->DefaultValue->BooleanValue);
								unset($input->DefaultValue->DateTimeValue);

								unset($input->Value->NumericValue);
								unset($input->Value->BooleanValue);
								unset($input->Value->DateTimeValue);

								unset($input->Min);
								unset($input->Max);
								break;
							case VariableType::Numeric:
								unset($input->DefaultValue->StringValue);
								unset($input->DefaultValue->BooleanValue);
								unset($input->DefaultValue->DateTimeValue);

								unset($input->Value->StringValue);
								unset($input->Value->BooleanValue);
								unset($input->Value->DateTimeValue);
								break;
						}

					default:
						self::clean($input->$key, $key);
						break;
				}
			}
		}
		if (is_array($input)) {
			foreach ($input as $key => $var) {
				self::clean($input[$key], $key);
			}
		}
	}

	public static function keepOnly(&$input, array $keys)
	{

		if (is_object($input)) {
			foreach (get_object_vars($input) as $key => $var) {
				if (in_array($key, $keys)) {
					self::keepOnly($input->$key, $keys);
				} else {
					unset($input->$key);
				}
			}
		}
		if (is_array($input)) {
			foreach ($input as $key => $var) {
				self::keepOnly($input[$key], $keys);
			}
		}
	}

	public static function getPropertyValues(&$input, array $properties, &$result = array())
	{

		if (is_object($input)) {
			foreach (get_object_vars($input) as $key => $var) {
				if (in_array($key, $properties)) {
					$result[] = $var;
				}
				self::GetPropertyValues($input->$key, $properties, $result);
			}
		}
		if (is_array($input)) {
			foreach ($input as $key => $var) {
				self::GetPropertyValues($input[$key], $properties, $result);
			}
		}
		return $result;
	}



	private static function isEmptyHtml($comment): bool
	{
		return strip_tags(str_replace(array("\r", "\n", "\t"), array("", "", ""), $comment)) === "";
	}

	/**
	 * @deprecated
	 */
	public static function GetSelectionGroupStatic($SelectionGroup, $CodeArray)
	{
		$currentCode = array_shift($CodeArray);
		foreach ($SelectionGroup->SelectionGroups as $selectionGroup) {
			if ($selectionGroup->Code == $currentCode) {
				if (count($CodeArray) > 0) {
					return self::GetSelectionGroup($selectionGroup, $CodeArray);
				} else {
					return $selectionGroup;
				}
			}
		}
	}


	/**
	 * @return ?VariableState
	 */
	public function GetVariable($Code)
	{
		foreach ($this->Sections as $Section) {
			foreach ($Section->Variables as $variable) {
				if ($variable->Name == $Code) {
					return $variable;
				}
			}
		}
		return null;
	}

	/**
	 * @return ?VariableState
	 */
	public function GetVariableByName($name){
		foreach ($this->Sections as $Section) {
			$result = $Section->GetVariableByName($name);
			if($result != null){
				return $result;
			}
		}
		return null;
	}


	public static function SetAdditionalDataForSections(&$Section, $partsByIdKey)
	{
		foreach ($Section->Sections as $SubSection) {
			self::SetAdditionalDataForSections($SubSection, $partsByIdKey);
		}
		self::SetAdditionalDataForSelectionGroups($Section->SelectionGroups, $partsByIdKey);
	}

	public static function SetAdditionalDataForSelectionGroups(&$SelectionGroups, $partsByIdKey)
	{
		foreach ($SelectionGroups as $selectionGroup) {
			self::SetAdditionalDataForSelectionGroups($selectionGroup->SelectionGroups, $partsByIdKey);
			self::SetAdditionalDataForSelectionGroupRows($selectionGroup->Rows, $partsByIdKey);
		}
	}

	private static function SetAdditionalDataForSelectionGroupRows(&$Rows, $partsByIdKey)
	{
		foreach ($Rows as $row) {
			if (array_key_exists($row->PartId, $partsByIdKey)) {
				$part = $partsByIdKey[$row->PartId];
				if ($part->Description != null) {
					$row->Description = $part->Description;
				}
				if ($part->PartNumber != null) {
					$row->PartNumber = $part->PartNumber;
				}
			}
		}
	}
}
