<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.UpdatePartConfigurationInstruction.html
 */
class UpdatePartConfigurationInstruction extends Base{

	/**
	 * The type of update that the instruction applies
	 * Variable: 0SelectionGroupRow: 1MainPart: 2Mandatory
	 * @required
	 */
	public int $Type;

	/**
	 * The variable update to apply
	 * Mandatory (if the instruction is of variable type)
	 */
	public VariableUpdate $Variable;

	/**
	 * The selection group row update to apply
	 * Mandatory (if the instruction is of selection group row type)
	 */
	public SelectionGroupRowUpdate $SelectionGroupRow;

	/**
	 * The main part update to apply
	 * Mandatory (if the instruction is of main part type)
	 */
	public MainPartUpdate $MainPart;

}