<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.MainPartUpdate.html
 */
class MainPartUpdate{

	/**
	 * Specifies the quantity to apply with the update.
	 */
	public ?float $Quantity;

}