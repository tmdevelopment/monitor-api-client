<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

use Exception;
use Monitor\API\Common\Comment;
use Monitor\Base;
use Monitor\VariableType;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.VariableState.html
 */
class VariableState extends Base
{

	/**
	 * CSharp type is Int64
	 * The business key identifier of the variable
	 * References IConfigurationVariable
	 */
	public string $Id;

	/**
	 * The human readable identifier of the variable
	 * 
	 */
	public string $Name;

	/**
	 * The description of the variable
	 * 
	 */
	public string $Description;

	/**
	 * The comment of the variable
	 * 
	 */
	public ?Comment $Comment;

	/**
	 * Specifies if the variable is mandatory
	 * 
	 */
	public bool $Mandatory;

	/**
	 * Specifies if the variable has a default value
	 * 
	 */
	public bool $HasDefaultValue;

	/**
	 * The minimum allowed numeric value of the variable
	 * 
	 */
	public ?float $Min;

	/**
	 * The maximum allowed numeric value of the variable
	 * 
	 */
	public ?float $Max;

	/**
	 * The rounding base (multiple) of the variable
	 * 
	 */
	public ?float $RoundingBase;

	/**
	 * The number of decimals used by the variable
	 * 
	 */
	public int $NumberOfDecimals;

	/**
	 * The business key identifier of the variable unit
	 * References Units
	 */
	public ?string $UnitId;

	/**
	 * The unit code of the variable
	 * 
	 */
	public ?string $UnitCode;

	/**
	 * The value type of the variable
	 * String: 0Numeric: 1Boolean: 2Date: 3
	 */
	public int $VariableType;

	/**
	 * The default value of the variable
	 * 
	 */
	public ?VariableValue $DefaultValue;

	/**
	 * The business key identifier of the extra field template used by the variable
	 * References ExtraFieldTemplates
	 */
	public ?int $ExtraFieldTemplateId;

	/**
	 * The variable formula
	 * 
	 */
	public ?string $Formula;

	/**
	 * The property binding type of the variable
	 * PartNumber: 0PartStandardPrice: 1PartWeight: 2SelectedQuantity: 3SelectedSetupQuantity: 4PartExtraField: 1000
	 */
	public ?int $BindPropertyType;

	/**
	 * The row index of the variable
	 * 
	 */
	public ?int $RowIndex;

	/**
	 * The selection comment of the variable
	 * 
	 */
	public ?Comment $SelectionComment;

	/**
	 * The selection type of the variable
	 * None: 0Initialization: 5RuleUnselect: 10Manual: 20GroupRule: 30SelectionRuleSelect: 40Locked: 50TemporaryLockedByManualSelection: 60
	 */
	public int $SelectionType;

	/**
	 * The value selection type of the variable
	 * Initialization: 0Manual: 1Formula: 2LinkedSelection: 3Fallback: 4
	 */
	public ?int $ValueSelectionType;

	/**
	 * The current value of the variable
	 * 
	 */
	public ?VariableValue $Value;

	/**
	 * Specifies if the variable is available
	 * 
	 */
	public bool $IsAvailable;

	/**
	 * The validation results of the variable
	 * 
	 * @var \Monitor\API\Infrastructure\ApiValidationResult[] $ValidationResults
	 */
	public array $ValidationResults;

	public function GetValue()
	{
		if ($this->Value == null) {
			return null;
		} else {
			switch ($this->VariableType) {
				case VariableType::Boolean:
					return $this->Value->BooleanValue;
				case VariableType::DateTime:
					return $this->Value->DateTimeValue;
				case VariableType::Numeric:
					return $this->Value->NumericValue;
				case VariableType::String:
					return $this->Value->StringValue;
			}
		}
		throw new Exception("Unhandled codepath");
	}
}
