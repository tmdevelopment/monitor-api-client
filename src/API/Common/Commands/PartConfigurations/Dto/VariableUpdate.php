<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.VariableUpdate.html
 */
class VariableUpdate extends Base{

	/**
	 * CSharp type is Int63
	 * The business key identifier of the variable to update.References IConfigurationVariableMandatory
	 * @required
	 */
	public string $VariableId;

	/**
	 * The value to apply with the update.Mandatory
	 * @required
	 */
	public VariableValue $Value;

}