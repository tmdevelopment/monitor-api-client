<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

use Monitor\API\Common\Comment;
use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.SectionState.html
 */
class SectionState extends Base
{

    use SelectionGroupIterator;

    /**
     * CSharp type is Int64
     * The component identifier of the section
     * 
     */
    public string $Id;

    /**
     * The description of the section
     * 
     */
    public string $Description;

    /**
     * The comment of the section
     * 
     */
    public ?Comment $Comment;

    /**
     * Specifies if the section is visible
     * 
     */
    public bool $Visible;

    /**
     * The sections that belong to the section
     * @var SectionState[] $Sections
     */
    public array $Sections;

    /**
     * The variables that belong to the section
     * @var VariableState[] $Variables
     */
    public array $Variables;

    /**
     * The selection groups that belong to the section
     * @var SelectionGroupState[] $SelectionGroups 
     */
    public array $SelectionGroups;

    /**
     * The row index of the section
     * 
     */
    public ?int $RowIndex;

    /**
     * Custom field, holding validation errors in flat array.
     */
    public array $ValidationErrors;

    public function GetVariableByName(string $name): ?VariableState
    {
        foreach ($this->Variables as $variable) {
            if ($variable->Name == $name) {
                return $variable;
            }
        }
        foreach ($this->Sections as $section) {
            $variable = $section->GetVariableByName($name);
            if ($variable != null) {
                return $variable;
            }
        }
    }
}
