<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

use Monitor\API\Common\Comment;
use Monitor\API\Common\Money;
use Monitor\Base;



/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.SelectionGroupRowState.html
 */
class SelectionGroupRowState extends Base
{

    /**
     * The alternative preparation code of the selection group row
     * 
     */
    public ?string $AlternativePreparationCode;

    /**
     * The business key identifier of the autocomplete configuration that is applicableto the selection group row
     * References AutoCompleteConfiguration
     */
    public ?string $AutoCompleteConfigurationId;

    /**
     * The comment of the selection group row
     * 
     */
    public ?Comment $Comment;

    /**
     * The default quantity of the selection group row
     * 
     */
    public ?float $DefaultQuantity;

    /**
     * The extra percent of the selection group row
     * 
     */
    public ?float $ExtraPercent;

    /**
     * Specifies if the selection group row has selections by default
     * 
     */
    public bool $HasSelections;

    /**
     * CSharp type is Int64
     * The business key identifier of the selection group row
     * References ISelectionGroupRowSnapshot
     */
    public string $Id;

    /**
     * The clone identifier of the selection group row
     * 
     */
    public int $CloneId;

    /**
     * The instruction comment of the selection group row
     * 
     */
    public ?Comment $Instruction;

    /**
     * Specifies if the selection group row specifies the default settings for theselection group
     * 
     */
    public bool $IsDefaultSettings;

    /**
     * Specifies if the selection group row is only used for calculations
     * 
     */
    public ?bool $IsForCalculationOnly;

    /**
     * The business key identifier of the main part of the selection group row
     * References Parts
     */
    public ?string $MainPartId;

    /**
     * The maximum allowed quantity of the selection group row
     * 
     */
    public ?float $MaxQuantity;

    /**
     * The minimum allowed quantity of the selection group row
     * 
     */
    public ?float $MinQuantity;

    /**
     * The business key identifier of the part of the selection group row
     * References Parts
     */
    public ?string $PartId;

    /**
     * The position of the selection group row
     * 
     */
    public ?string $Position;

    /**
     * The quantity forumla of the selection group row
     * 
     */
    public ?string $QuantityFormula;

    /**
     * The row index of the selection group row
     * 
     */
    public int $RowIndex;

    /**
     * The setup quantity of the selection group row
     * 
     */
    public ?float $SetupQuantity;

    /**
     * The setup quantity formula of the selection group row
     * 
     */
    public ?string $SetupQuantityFormula;

    /**
     * The to operation of the selection group row
     * 
     */
    public ?int $ToOperation;

    /**
     * The discount percentage of the selection group row
     * 
     */
    public float $DiscountPercentage;

    /**
     * Specifies if discount is locked for the selection group row
     * 
     */
    public bool $LockedDiscount;

    /**
     * Specifies if unit price is locked for the selection group row
     * 
     */
    public bool $LockedUnitPrice;

    /**
     * The price factor of the selection group row
     * 
     */
    public float $PriceFactor;

    /**
     * The price formula text of the selection group row
     * 
     */
    public ?string $PriceFormulaText;

    /**
     * The quantity of the selection group row
     * 
     */
    public float $Quantity;

    /**
     * The selection comment of the selection group row
     * 
     */
    public ?Comment $SelectionComment;

    /**
     * The selection type of the selection group row
     * None: 0Initialization: 5RuleUnselect: 10Manual: 20GroupRule: 30SelectionRuleSelect: 40Locked: 50TemporaryLockedByManualSelection: 60
     */
    public int $SelectionType;

    /**
     * The calculated unit price of the selection group row
     * 
     */
    public Money $UnitPrice;

    /**
     * The calculated unit price of the selection group row in the company currency
     * 
     */
    public Money $UnitPriceInCompanyCurrency;

    /**
     * Specifies if the selection group row is selected
     * 
     */
    public bool $IsSelected;

    /**
     * Specifies if the selection group row is available for selection
     * 
     */
    public bool $IsAvailable;

    /**
     * The validation results of the selection group row
     * 
     */
    public array $ValidationResults;



    public string $PartNumber;
    public string $Description;
}
