<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.SelectionGroupRowUpdate.html
 */
class SelectionGroupRowUpdate{
	/** CSharp type is Int64
	 * @required
	 */
	public string $SelectionGroupRowId;
	public int $CloneId;
	public int $SelectionLocking;
	public ?int $PartId;
	public ?bool $Selected;
	public ?float $Quantity;

}