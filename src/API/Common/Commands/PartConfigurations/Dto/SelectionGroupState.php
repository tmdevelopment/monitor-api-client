<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

use Monitor\API\Common\Comment;
use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.SelectionGroupState.html
 */
class SelectionGroupState extends Base
{

    use SelectionGroupIterator;

    /**
     *  CSharp type is Int64
     *  The component identifier of the selection group
     * 
     */
    public string $Id;

    /**
     * Specifies if selected quantity can be edited on the selection group
     * 
     */
    public bool $CanEditSelectedQuantity;

    /**
     * The human readable identifier of the selection group
     * 
     */
    public string $Code;

    /**
     * The comment of the selection group
     * 
     */
    public ?Comment $Comment;

    /**
     * The description of the selection group
     * 
     */
    public string $Description;

    /**
     * The maximum allowed selected quantity of the selection group
     * 
     */
    public ?float $MaximumSelectedQuantity;

    /**
     * The maximum amount of selected rows allowed on the selection group
     * 
     */
    public ?int $MaximumSelectedRows;

    /**
     * The minimum allowed selected quantity of the selection group
     * 
     */
    public ?float $MinimumSelectedQuantity;

    /**
     * The minimum amount of selected rows allowed on the selection group
     * 
     */
    public ?int $MinimumSelectedRows;

    /**
     * The row index of the selection group
     * 
     */
    public ?int $RowIndex;

    /**
     * The rows of the selection group
     * @var SelectionGroupRowState[] $Rows
     */
    public array $Rows;

    /**
     * The states of the child selection groups
     * @var SelectionGroupState[] $SelectionGroups
     */
    public array $SelectionGroups;

    /**
     * The source version of the selection group
     * 
     */
    public int $SourceVersion;

    /**
     * Specifies if the selection group is available
     * 
     */
    public bool $IsAvailable;

    /**
     * The validation results of the selection group
     * 
     * @var \Monitor\API\Infrastructure\ApiValidationResult[] $ValidationResults
     */
    public array $ValidationResults;

    /**
     * @return ?SelectionGroupRowState
     */
    public function GetSelectedRow()
    {
        foreach ($this->Rows as $Row) {
            if ($Row->IsSelected && $Row->IsAvailable) {
                return $Row;
            }
        }
        return null;
    }

    /**
     * @return SelectionGroupRowState[]
     */
    public function GetSelectedRows()
    {
        $result = array();
        foreach ($this->Rows as $Row) {
            if ($Row->IsSelected && $Row->IsAvailable) {
                $result[] = $Row;
            }
        }
        return $result;
    }

    public function GetRow(string $PartNumber)
    {
        $result = $this->GetRows([$PartNumber]);
        return array_pop($result);
    }

    public function GetRows(array $PartNumbers)
    {
        $result = array();
        foreach ($this->Rows as $Row) {
            if (in_array($Row->PartNumber, $PartNumbers)) {
                $result[] = $Row;
            }
        }
        return $result;
    }
}
