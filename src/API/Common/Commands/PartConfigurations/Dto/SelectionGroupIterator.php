<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

trait SelectionGroupIterator
{
    /**
     * @var string|array codes
     */
    public function GetSelectionGroup($codes): ?SelectionGroupState
    {
        if (isset($this->Sections)) {
            foreach ($this->Sections as $section) {
                $selectionGroup = $section->GetSelectionGroup($codes);
                if ($selectionGroup != null) {
                    return $selectionGroup;
                }
            }
        }

        $codes = is_array($codes) ? $codes : array($codes);
        $currentCode = array_shift($codes);
        if (isset($this->SelectionGroups)) {
            foreach ($this->SelectionGroups as $selectionGroup) {
                if ($selectionGroup->Code == $currentCode) {
                    if (count($codes) > 0) {
                        return $selectionGroup->GetSelectionGroup($codes);
                    } else {
                        return $selectionGroup;
                    }
                }
            }
        }
        return null;
    }
}
