<?php

namespace Monitor\API\Common\Commands\PartConfigurations\Dto;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.PartConfigurations.Dto.VariableValue.html
 */
class VariableValue{

	/**
	 * The value type of the typed value.String: 0Numeric: 1Boolean: 2Date: 3
	 */
	public int $Type;

	/**
	 * The string value of the typed value.
	 */
	public ?string $StringValue;

	/**
	 * The boolean value of the typed value.
	 */
	public ?bool $BooleanValue;

	/**
	 * The numeric value of the typed value.
	 */
	public ?float $NumericValue;

	/**
	 * The date/time value of the typed value.
	 */
	public \DateTime $DateTimeValue;

}