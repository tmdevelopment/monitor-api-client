<?php

namespace Monitor\API\Common\Commands\Shared;

use Monitor\Base;

/**
 * Class definition: 
 * https://api.monitor.se/api/Monitor.API.Common.Commands.Shared.SetAddressCountry.html
 */
class SetAddressCountry extends Base
{

    /**
     * The format type of the address
     * Mandatory
     * @required
     */
    public int $AddressFormatType;

    /**
     * The business key identifier of the language code
     * References LanguageCodes.Mandatory
     * @required
     */
    public string $LanguageId;

    /**
     * References Countries
     * Mandatory
     * @required
     */
    public string $CountryId;
}
