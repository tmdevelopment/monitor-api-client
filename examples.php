<?php

declare(strict_types=1);
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, "vendor", "autoload.php"));

use \YaLinqo\Enumerable;
use Monitor\{Client, OrderRowType, PartConfigurationUpdateType, VariableType};
use Monitor\API\Common\Commands\PartConfigurations\{CreateForCustomerOrder, CreatePartConfigurationForQuote, GetPartConfiguration, PartConfigurationSessionExists, PokePartConfigurationSession, UpdatePartConfiguration};
use Monitor\API\Common\Commands\PartConfigurations\Dto\{UpdatePartConfigurationInstruction, PartConfigurationState, VariableValue, VariableUpdate};
use Monitor\API\Sales\Commands\CustomerOrders\AddCustomerOrderRow;
use Monitor\API\Sales\Commands\CustomerOrders\CreateCustomerOrder;



$g5 = new Client($host = "https://host:8001", $company = "001.1", $user = "", $password = "");


/**
 * Get Part by id
 */
$part = $g5->Inventory_Parts()->get(10559);

/**
 * Get Parts (top 25)
 */ 
$parts25 = $g5->Inventory_Parts()->get();

/**
 * Get Parts (top 50)
 */ 
$parts50 = $g5->Inventory_Parts()->top(50)->get();

/**
 * Filter by field, eg by PartNumber
 */
$filteredPartByPartNumber = $g5->Inventory_Parts()->filter("PartNumber eq 'LO1.IL.AM22H2.R0'")->get();

/**
 * Like SQL LIMIT, default Client value is 25
 */
$top7Parts = $g5->Inventory_Parts()->top(7)->get();

/**
 * Like SQL LIMIT and OFFSET
 */
$top6PartsSkip7 = $g5->Inventory_Parts()->top(6)->skip(7)->get();

/**
 * Some fields or fieldblocks are not returned by default, then expand querypart has to be used
 */
$expandedPartPlanningInformations = $g5->Inventory_Parts()->expand("PartPlanningInformations")->get(10559);




/**
 *  Get Customers
 */
$customers = $g5->Sales_Customers()->get();

/**
 * Get Customer by id
 */
$customer1076 = $g5->Sales_Customers()->get(1076);

/**
 * Get currencies
 */
$currencies = $g5->Common_Currencies()->get();


/**
 * Get currencies and map to a type
 */
$currencies = $g5->mapTo(Currency::class)->Common_Currencies()->get();

//filter with Yalingo - not sure if it will be useful
$eur = from($currencies)->firstOrDefault(null, function ($currency) {
    return $currency->Code == "EUR";
});


/**
 * Get Customer Order Types
 */
$customerOrderTypes = $g5->Sales_CustomerOrderTypes()->get();

/**
 * build a request and Get Configuration For Customer Order, returns sessionid, 
 */
$createForCustomerOrder = new CreateForCustomerOrder();
$createForCustomerOrder->PartId = 10559;
$createForCustomerOrder->Quantity = 1;
$createForCustomerOrder->CurrencyId = intval($eur->Id);
$createForCustomerOrder->CustomerId = 1076;
$createForCustomerOrder->CustomerOrderTypeId = intval($customerOrderTypes[2]->Id);
$sessionId = $g5->Sales_PartConfigurations_CreateForCustomerOrder($createForCustomerOrder);

/**
 * Check If Session Exists
 */
if (isset($sessionId)) {
    $exists = new PartConfigurationSessionExists();
    $exists->SessionId = $sessionId;
    $sessionExists = $g5->Common_PartConfigurations_Exists($exists);
    if ($sessionExists == false) {
        unset($sessionId);
    }
}

/**
 * Poke - should be possible to extend the session lifetime, throws error if it has been expired
 */
$poke = new PokePartConfigurationSession();
$poke->SessionId = $sessionId;
try {
    $g5->Common_PartConfigurations_Poke($poke);
} catch (Exception $ex) {
    unset($sessionId);
}

/**
 * Get PartConfigurationState
 */
$getPartConfiguration = new GetPartConfiguration();
$getPartConfiguration->SessionId = $sessionId;
$configuration = $g5->Common_PartConfigurations_Get($getPartConfiguration);


/**
 * filter variables fith Yalingo
 */
$variablesSection = from($configuration->Sections)->firstOrDefault(null, function ($section) {
    return $section->Description == "<Variables>";
});
/**
 * Get the variable by name
 */
$W1_input = from($variablesSection->Variables)->firstOrDefault(null, function ($variable) {
    return $variable->Name == "W1_INPUT";
});

/**
 * Update variable value
 */ 
$updatePartConfiguration = new UpdatePartConfiguration();
$updatePartConfiguration->SessionId = $sessionId;

$W1_update = new UpdatePartConfigurationInstruction();
$W1_update->Type = PartConfigurationUpdateType::Variable;
$W1_update->Variable = new VariableUpdate();
$W1_update->Variable->VariableId = 485;
$W1_update->Variable->Value = new VariableValue();
$W1_update->Variable->Value->Type = VariableType::Numeric;
$W1_update->Variable->Value->NumericValue = 2000;
$updatePartConfiguration->Instructions[] = $W1_update;
$configuration = $g5->Common_PartConfigurations_Update($updatePartConfiguration);

/**
 * Currently the configuration is invalid by default,
 * check if its valid, if not make selections until its valid
 */

if ($configuration->IsValid == false) {

    $killCounter = 0;
    while (!empty($invalidStates = PartConfigurationState::getVariablesAndSelectionGroupsWithValidationErrors($configuration))) {
        $updatePartConfiguration = PartConfigurationState::getFirstRowSelectedUptateInformation($invalidStates[0], $sessionId);
        $configuration = $g5->mapTo(PartConfigurationState::class)->Common_PartConfigurations_Update($updatePartConfiguration);
        $killCounter++;
        if ($killCounter > 10) {
            throw new Exception("too many times");
        }
    }
}


/**
 * if configuration is valid continue
 */
if ($configuration->IsValid == false) {
    throw new Exception("Config is invalid");
} else {

    /**
     * Calculate the total price - its combined of UnitPrice from product and from Configuration
     * Might have to provide array of sales prices
     */
    $totalPrice = $configuration->GetPrice();
    

    /**
     * Create Customer Order
     */
    $customerOrder = new CreateCustomerOrder();
    $customerOrder->CustomerId = 1076;
    //eg here, OrderNumber has @maxlength 15 defined - monitor DB has varchar(15) column
    $customerOrder->OrderNumber = "TEST-" . date("dm_Hi");

    $customerOrderRow = new AddCustomerOrderRow();
    $customerOrderRow->CustomerOrderId = 0;
    $customerOrderRow->PartId = 10559;
    $customerOrderRow->PartConfigurationSessionId = $sessionId;
    $customerOrderRow->OrderRowType = OrderRowType::Part;

    $customerOrder->Rows[] = $customerOrderRow;
    $response = $customerOrder = $g5->Sales_CustomerOrders_Create($customerOrder);
    if (isset($response->EntityId)) {
        echo "Order created\n";
    }
}
